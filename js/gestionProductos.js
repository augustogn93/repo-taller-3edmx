var productosObtenidos;

function getProductos() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           //console.log(request.responseText);
           productosObtenidos = request.responseText;
           procesarProductos();
        }
    };
    request.open("GET", url, true);
    request.send();
}

function procesarProductos() {
    var JSONProducts = JSON.parse(productosObtenidos);
    //alert(JSONProducts.value[0].ProductName);
    var divTabla = document.getElementById("divTableProducts");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (var i = 0; i < JSONProducts.value.length; i++ ) {
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerHTML = JSONProducts.value[i].ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerHTML = JSONProducts.value[i].UnitPrice;
        var columnaStock = document.createElement("td");
        columnaStock.innerHTML = JSONProducts.value[i].UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}

var ClientesObtenidos;

function getClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           //console.log(request.responseText);
           ClientesObtenidos = request.responseText;
           procesarClientes();
        }
    };
    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {
    var JSONClientes = JSON.parse(ClientesObtenidos);
    //alert(JSONClientes.value[0].ContactName);
    var divTabla = document.getElementById("divTableCustomers");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (var i = 0; i < JSONClientes.value.length; i++ ) {
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerHTML = JSONClientes.value[i].ContactName;
        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerHTML = JSONClientes.value[i].City;
        var columnaBandera = document.createElement("td");
        var country = JSONClientes.value[i].Country;
        if (country == "UK") {
            country = "United-Kingdom"
        }
        var imgBandera = document.createElement("img");
        var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+country+".png"
        imgBandera.src = rutaBandera;
        imgBandera.classList.add("flag");
        columnaBandera.appendChild(imgBandera);
        

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaBandera);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}

function getClientesAlemanes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?filter=Country eq 'Germany'";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
           console.log(request.responseText);
        }
    };
    request.open("GET", url, true);
    request.send();
}